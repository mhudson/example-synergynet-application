package au.net.michaelhudson;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import multiplicity3.appsystem.IMultiplicityApp;
import multiplicity3.appsystem.IQueueOwner;
import multiplicity3.appsystem.MultiplicityClient;
import multiplicity3.csys.MultiplicityEnvironment;
import multiplicity3.csys.factory.ContentTypeNotBoundException;
import multiplicity3.csys.factory.IContentFactory;
import multiplicity3.csys.items.image.IImage;
import multiplicity3.csys.items.shapes.IColourRectangle;
import multiplicity3.csys.stage.IStage;
import multiplicity3.input.IMultiTouchEventListener;
import multiplicity3.input.MultiTouchInputComponent;
import multiplicity3.input.events.MultiTouchCursorEvent;
import multiplicity3.input.events.MultiTouchObjectEvent;
import synergynet3.SynergyNetApp;

import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Michael Hudson on 6/04/2015.
 */
public class Application extends SynergyNetApp implements IMultiTouchEventListener {

    private IStage stage;
    private static Logger logger;

    public static void main(String[] args) {
        logger = Logger.getLogger("log4j");
        MultiplicityClient client = MultiplicityClient.get();
        client.start();
        Application app = new Application();
        client.setCurrentApp(app);
    }

    public void cursorReleased(MultiTouchCursorEvent multiTouchCursorEvent) {
        logger.info("Released");
        UUID id = UUID.randomUUID();
        try {
            IColourRectangle box = stage.getContentFactory().create(IColourRectangle.class, "box-" + id.toString(), id);
            stage.addItem(box);
            box.setSize(40, 40);
            box.setSolidBackgroundColour(ColorRGBA.randomColor());
            box.setWorldLocation(stage.tableToWorld(new Vector2f(multiTouchCursorEvent.getPosition())));
            logger.fine("Cursor released at " + multiTouchCursorEvent.getPosition().toString());
        } catch (ContentTypeNotBoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getFriendlyAppName() {
        return "Example SynergyNet Application";
    }

    @Override
    public void shouldStart(MultiTouchInputComponent multiTouchInputComponent, IQueueOwner iQueueOwner) {
        stage = MultiplicityEnvironment.get().getLocalStages().get(0);
        stage.getZOrderManager().setAutoBringToTop(false);
        IContentFactory contentFactory = stage.getContentFactory();
        try {
            IImage background = contentFactory.create(IImage.class, "background", UUID.randomUUID());
            stage.addItem(background);
            background.setImage("exampleapp/images/face.jpg");
            background.setRelativeLocation(new Vector2f(0, 0));
            background.setSize(stage.getDisplayWidth(), stage.getDisplayHeight());
            logger.info("Application ready");
        } catch (ContentTypeNotBoundException e) {
            e.printStackTrace();
        }

        multiTouchInputComponent.registerMultiTouchEventListener(this);
    }

    @Override
    public void shouldStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void cursorPressed(MultiTouchCursorEvent multiTouchCursorEvent) {

    }

    @Override
    public void cursorClicked(MultiTouchCursorEvent multiTouchCursorEvent) {

    }

    @Override
    public void cursorChanged(MultiTouchCursorEvent multiTouchCursorEvent) {

    }

    @Override
    public void objectAdded(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    @Override
    public void objectRemoved(MultiTouchObjectEvent multiTouchObjectEvent) {

    }

    @Override
    public void objectChanged(MultiTouchObjectEvent multiTouchObjectEvent) {

    }
}
